# How to deploy a React app with AWS Amplify
AWS Amplify is an easy way to deploy a web app by automatically deploying code pushed to a gitlab repo

## Steps to setup
Reference [reference this guide](https://docs.amplify.aws/start/q/integration/react/)
On your browser:
- login to aws and navigate to Amplify
- create new app and hook it up to your gitlab repo

In VS Code:
- run `touch amplify.yml` and paste this:
```
version: 1
env:
  variables:
    key: value
backend:
  phases:
    build:
      commands:
        - amplifyPush --simple
frontend:
  phases:
    preBuild:
      commands:
        - cd frontend/<react app name>
        - npm ci
    build:
      commands:
        - npm run build
  artifacts:
    baseDirectory: frontend/<react app name>/build
    files:
      - '**/*'
```
- run `amplify init`
- run `amplify add api`
    - make sure to select `REST` as api type
    - select `Express App` as the app type
- run `amplify configure`
- run `amplify push` to push the api to AWS

## Debugging
Debug the backend API via `amplify mock function <function name>`

## Troubleshooting
1.
**local-aws-info.json is corrupted**
- https://stackoverflow.com/a/64480614

2.
If you get CORS errors, add this to the `app.js` before any routes:
```
app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");

    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
```

3.
Calling amplify deployed express API from amplify deployed react app causes `403` error:
- `get` routes are weird in amplify and you should stick to `post` routes whenever possible. `get` routes require more config to use url params so its best to avoid them
